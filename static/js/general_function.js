function getEditorSelector() {
  return $("#editor-content");
}

function insert_row_r(data, i) {
  var row = document.getElementById("editor-content");
  var x = row.insertRow();
  x.innerHTML="<td id='sentence'>" + data[0] + "</td><td id='entity_1'>" + data[1] + "</td><td id='entity_2'>" + data[2] + "</td>" + "<td id='relation'><select class='selection' id='rel_" + i + "'>" + rid + "</select></td>"
}

function insert_row_extend(data, i) {
  var row = document.getElementById("editor-content");
  var x = row.insertRow();
  x.innerHTML="<td id='sentence'>" + data[0] + "</td><td id='entity_1'>" + data[1] + "</td><td id='entity_2'>" + data[2] + "</td>" + "<td id='relation'><select class='selection' id='rel_" + i + "'>" + rid + "</select></td>"
  $('#rel_' + i + ' option[value="' + data[3] + '"]').attr("selected", "selected");
}

// remove class from tag
function chin_chow(string) {
  return string.replace(new RegExp(/\sclass=...../, 'igm'), '');
}


function color_of_tag_raw(string) {
  // get tag sample
  var tagSample = [];
  tagSample.push(string[1].slice(1, 4))
  tagSample.push(string[2].slice(1, 4))

  // change tag class in sentence
  for (var i = 0; i < tagSample.length; i++) {
    var pattern = string[i + 1];
    var rl = '<' + tagSample[i] + ' class="' + tagSample[i].toLowerCase() + '">' + string[i + 1].slice(5,);
    string[0] = string[0].replace(new RegExp(pattern, 'g'), rl)
  }

  // change tag class in entities
  for (var i = 0; i < tagSample.length; i++) {
    var rl = tagSample[i] + ' class="' + tagSample[i].toLowerCase() + '"';
    string[i + 1] = string[i + 1].replace(new RegExp(tagSample[i], 'g'), rl)
  }
  return string
}

function color_of_tag_done(string) {
  // get tag sample
  var tagSample = [];
  tagSample.push(string[1].slice(1, 4))
  tagSample.push(string[2].slice(1, 4))

  // change tag class in sentence
  for (var i = 0; i < tagSample.length; i++) {
    var pattern = '<' + tagSample[i].toLowerCase() + '>' + string[i + 1].slice(5, string[i + 1].length - 6);
    console.log(pattern)
    var rl = '<' + tagSample[i] + ' class="' + tagSample[i].toLowerCase() + '">' + string[i + 1].slice(5,);
    string[0] = string[0].replace(new RegExp(pattern, 'g'), rl)
  }

  // change tag class in entities
  for (var i = 0; i < tagSample.length; i++) {
    var rl = tagSample[i] + ' class="' + tagSample[i].toLowerCase() + '"';
    string[i + 1] = string[i + 1].replace(new RegExp(tagSample[i], 'g'), rl)
  }
  return string
}

function getDoc(doc_name) {
  $('#myPleaseWait').modal('show');
  $.get("/get_doc/" + doc_name, function (response) {
    var responseJSON = JSON.parse(response);
    var docDetail = responseJSON['data'];
    var content = docDetail['content'];
    // var editorSelector = getEditorSelector();
    document.getElementById("editor-content").innerHTML = ""; 
    currentDocName = docDetail['doc_name'];
    $("#current-message").text('Working on: ' + currentDocName);
    cursorElements = [];
    // console.log('real 0' + sele[2]);
    for (var i = 0; i < content.length; i++) {
      insert_row_r(color_of_tag_raw(content[i]), i + 1)
    } 
  }).fail(function () {
    alert("Loading Error");
  }).always(function () {
    $('#myPleaseWait').modal('hide');
  });
}

function getDocDone(doc_name) {
  $('#myPleaseWait').modal('show');
  $.get("/get_doc_done/" + doc_name, function (response) {
    var responseJSON = JSON.parse(response);
    var docDetail = responseJSON['data'];
    console.log(docDetail)
    var content = docDetail['content'];
    console.log(content)
    // var editorSelector = getEditorSelector();
    document.getElementById("editor-content").innerHTML = ""; 
    currentDocName = docDetail['doc_name'];
    $("#current-message").text('Working on: ' + currentDocName);
    cursorElements = [];
    for (var i = 0; i < content.length; i++) {
      insert_row_extend(color_of_tag_done(content[i]), i+1)
    } 
  }).fail(function () {
    alert("Loading Error");
  }).always(function () {
    $('#myPleaseWait').modal('hide');
  });
}


function generateDoneList(doc_names) {
  for (var i = 0; i < doc_names.length; i++) {
    $("#file-done").append('<li class="list-group-item docs-item" onclick="getDocDone(\'' + doc_names[i] + '\')">' + doc_names[i] + '</li>')
  }
}

function generateRawList(doc_names, doc_done) {
  for (var i = 0; i < doc_names.length; i++) {
    if (doc_done.indexOf(doc_names[i]) > -1) {
      $("#file-raw").append('<li class="list-group-item docs-item list-group-item-success" onclick="getDoc(\'' + doc_names[i] + '\')">' + doc_names[i] + '</li>');
    } else {
      $("#file-raw").append('<li class="list-group-item docs-item" onclick="getDoc(\'' + doc_names[i] + '\')">' + doc_names[i] + '</li>');
    }
  }
}

function getContent() {
  var i = 1;
  var count = 0;
  var idid;
  var content = [];
  var row = [];

  $('#editor-content tr').each(function() {
    $(this).find('td').each(function () {
      if (count == 0) {
        row.push(chin_chow($(this).html()))
        count += 1
      }
      else
      if (count < 3) {
        row.push(chin_chow($(this).html()))
        count += 1
      }
      else {
        idid = "rel_" + i
        row.push(document.getElementById(idid).value)
        // console.log('count = 3: ' + content)
        content.push(row)
        row = []
        count = 0
        return;
      }
    })
    i += 1
  });
  console.log(content)
  return content;
}
function getCursorPos(){
  var editorSelector = getEditorSelector();
  var cursorStart = editorSelector.prop("selectionStart");
  var cursorEnd   = editorSelector.prop("selectionEnd");

  return {
    cursorStart: cursorStart,
    cursorEnd: cursorEnd
  }
}

function saveDoc() {
  if ($.trim($("#editor-content").text()) == '') {
    return
  }
  content = getContent()
  $.ajax({
      url: '/save_doc',
      type: 'post',
      dataType: 'json',
      success: function () {
        alert("Saving success");
        location.reload();
      },
      fail: function() {
        alert("Saving error");
      },  
      data: {
        'content': JSON.stringify(content),
        'doc_name': currentDocName
      }
  });
}
function addRelation() {
  var re = $('#newRelation').val();
  $.get("/addrelation/" + re, function (response) {
    var responseJSON = JSON.parse(response);
    var re = responseJSON['re'];
    console.log(re)
  }).fail(function () {
    alert("Loading Error");
  }).always(function () {
    $('#myPleaseWait').modal('hide');
  });
}

function generateRelation2id() {
  var row = [];
  $.get("/read_rid/", function (response) {
    var data = JSON.parse(response);
    var len = data['len'];
    relation2id = data['data'];
    for (var i = 0; i < len; i++) {
      row.push("<option value='" + i + "'>" + relation2id[i] + "</option>");
    }
    rid = row;
  });
}