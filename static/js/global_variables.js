var cursorElements  = [];
var tagCSSClasses   = ['tag-per', 'tag-org', 'tag-loc', 'tag-time', 'tag-num', 'tag-mon', 'tag-percent', 'tag-prod'];
var tagNotations    = ['PER', 'ORG', 'LOC', 'TIM', 'NUM', 'MON', 'PCT', 'PRD'];
var tagName         = ['PERSON', 'ORGANIZATION', 'LOCATION', 'TIME', 'NUMBER', 'MONEY', 'PERCENT', 'PRODUCT'];
var tagColors       = ['#DA70D6', '#00FA9A', '#ec4a4a', '#FFA07A', '#ecec69', '#ADFF2F', '#1E90FF', '#19cece'];
var tagHotKeys      = ['alt+1', 'alt+2', 'alt+3', 'alt+4', 'alt+5', 'alt+6', 'alt+7', 'alt+8'];

var content = '';
var info = {};
var currentDocName = '';
var myApp = '';

var relation2id = {};
var rid = '';