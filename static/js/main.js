$(document).ready(function () {
    (function () {
        var tagHTMLGenerated = '';
        for (var i = 0; i < tagCSSClasses.length; i++) {
            tagHTMLGenerated = '<p style="background-color: ' + tagColors[i] + '" class="tag-btn btn btn-xs" onclick="tag(\'' + tagCSSClasses[i] + '\')">' + tagName[i] + '</p>';

            $("#tag-container").append(tagHTMLGenerated);
        }
    })();
    generateRelation2id();
    setInterval(function () {
        var editorSelect = getEditorSelector();
        var content = editorSelect.val();

        $("#content").text(content);
        if (cursorElements.length == 0) {
            return
        }

        clonedCursorElements = cursorElements.slice(0);
        clonedCursorElements.sort(function (a, b) {
            return a['start'] - b['start']
        });

        var prefixFinalContent = content.slice(0, clonedCursorElements[0]['start']);
        var finalContentChunks = [];

        var currentContent = '';
        var currentTag = '';
        var finalContentChunk = '';

        for (var i = 0; i < clonedCursorElements.length; i++) {
            if (clonedCursorElements[i + 1]) {
                currentContent = content.slice(clonedCursorElements[i]['start'], clonedCursorElements[i + 1]['start']);
                currentWord = content.slice(clonedCursorElements[i]['start'], clonedCursorElements[i]['end']);
                currentTag = mapClassToTag(clonedCursorElements[i]['class']);
                finalContentChunk = addTagToText(currentTag, currentWord) + currentContent.slice(clonedCursorElements[i]['end'] - clonedCursorElements[i]['start'], currentContent.length)

                finalContentChunks.push(finalContentChunk);
            } else {
                currentContent = content.slice(clonedCursorElements[i]['start'], content.length);
                currentWord = content.slice(clonedCursorElements[i]['start'], clonedCursorElements[i]['end']);
                currentTag = mapClassToTag(clonedCursorElements[i]['class']);
                finalContentChunk = addTagToText(currentTag, currentWord) + currentContent.slice(clonedCursorElements[i]['end'] - clonedCursorElements[i]['start'], currentContent.length)

                finalContentChunks.push(finalContentChunk);
            }
        }

        finalContent = prefixFinalContent + finalContentChunks.join('');
        $("#content").text(finalContent);

    }, 500);

    var editorSelector = getEditorSelector();
    editorSelector.mouseup(function () {
        var cursorPos = getCursorPos();
        var text = editorSelector.val();

        for (var i = 0; i < cursorElements.length; i++) {
            if (cursorElements[i]['start'] <= cursorPos['cursorStart'] && cursorPos['cursorStart'] <= cursorElements[i]['end']) {
                $("#current-tag").html('(' + text.slice(cursorElements[i]['start'], cursorElements[i]['end']) + ') as tag (' + mapClassToTag(cursorElements[i]['class']) + ')');
                return;
            }
        }
        $("#current-tag").html("");
    });

    var htmlGenerated = [];
    var preJ = 0;
    for (i = 0; i < (tagHotKeys.length / 4).toFixed(); i++) {
        htmlGenerated.push("<div class='row'>");
        
        for (j = preJ; j < tagHotKeys.length; j++) {
            if (j % 4 == 0 && j != preJ) {    
                break;
            }
            htmlGenerated.push("<p class='col-md-3'>" + tagHotKeys[j] + " - tag " + tagNotations[j] + "</p>");
        }
        preJ = j;
        htmlGenerated.push('</div>');
    }
    $("#tag-hotkeys-container").append(htmlGenerated.join(''));

    $.each(tagHotKeys, function (index, element) {
        editorSelector.bind('keydown', element, function () {
            tag(tagCSSClasses[index]);
        })
    });

    editorSelector.bind('keydown', 'del', function () {
        removeTag();
    });
});