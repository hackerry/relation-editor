
- Requirement:

	+ Mongodb v3.4
	+ Docker, Docker-compose
	+ python 3

- files_raw folder: contain raw files
- files_done folder: contain done file after run export operation

- Runing:
	+ Fill files_raw folder with raw file that you want to edit the relation, file format (tab separated value)
	+ Start mongod service
	+ cd to RE directory
	+ Run: make run
	+ Import raw data to database
		- Go to http://0.0.0.0:7000/tools/import
	+ Go to http://0.0.0.0:7000/
	+ Done

- Save edited file: click Save button, the file will be marked as done file, ready to be exported

- Export done files:
	+ Go to http://0.0.0.0:7000/tools/export


	
	