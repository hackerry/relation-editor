import json, csv
import os
import codecs
import zipfile
import time
import datetime

from flask import jsonify
from flask import send_file
from flask import Flask, redirect
from flask import render_template
from flask import request
from flask_cors import CORS
from pymongo import MongoClient

app = Flask(__name__)
app.debug = True
CORS(app)

host = '0.0.0.0'
port = '7000'

file_done_folder = os.environ['FILE_DONE_FOLDER']
file_raw_folder = os.environ['FILE_RAW_FOLDER']
MONGODB_HOST = os.environ['MONGODB_HOST']
MONGODB_PORT = int(os.environ['MONGODB_PORT'])

#training_tasks = []
#crawling_tasks = {}

client = MongoClient(MONGODB_HOST, MONGODB_PORT)
db = client['ne_editor']

def get_rows_from_file(filename):
    content = []
    with open (filename, 'r') as data_file:
        data = csv.reader(data_file, delimiter='\t')
        for line in data:
            content.append(list(line))
    return content
def convert_doc_content_to_write(data):
    content = u''
    for row in data:
        content += row[0] + '\t' + row[1] + '\t' + row[2] + '\t' + row[3] + '\n'
    return content
    
def get_file_content(filename):
    file = open(filename, 'r')
    content = file.read()
    file.close()
    return content


def zipdir(path, ziph):
    for root, dirs, files in os.walk(path):
        for file in files:
            ziph.write(os.path.join(root, file))


def put_file_content(filename, data):
    wfile = codecs.open(filename, "w", "utf-8")
    wfile.write(data)
    wfile.close()


def convert_date_to_timestamp(date_string):
    dt = datetime.datetime.strptime(date_string, "%Y-%m-%d")
    return int(time.mktime(dt.timetuple()) + (dt.microsecond / 1000000.0))


@app.route('/read_rid/', methods=['GET'])
def read_relationID_file():
    relation2id = {}
    # pair = []
    with open ('relation2id.txt') as file:
        for line in file:
            pair = line.strip().split()
            relation2id[str(pair[1])] = pair[0]
    return json.dumps({'data': relation2id, 'len': len(relation2id)})

@app.route('/<path:path>')
def static_file(path):
    return app.send_static_file(path)


@app.route('/', methods=['GET'])
def index():
    docs_done = db.docs_done.find()
    docs = db.docs.find()
    docs_done_name = list(map(lambda doc: doc['doc_name'], docs_done))
    docs_name = list(map(lambda doc: doc['doc_name'], docs))
    return render_template('index.html', docs_name=docs_name, docs_done_name=docs_done_name)


@app.route('/tools/import', methods=['GET'])
def tool_import():
    print (os.path)
    text_files = []
    try:
        if not os.path.exists(file_raw_folder):
            os.makedirs(file_raw_folder)

        text_files = [f for f in os.listdir(file_raw_folder) if f.endswith('.txt')]
        db.docs.remove({'doc_name': {'$in': text_files}})

        bulk = db.docs.initialize_ordered_bulk_op()
        for text_file in text_files:
            data = {
                "doc_name": text_file,
                "content": get_rows_from_file('{}/{}'.format(file_raw_folder, text_file)),
                "created_at": int(time.time()),
                "updated_at": int(time.time())
            }
            if len(data['content']) > 3:
                bulk.insert(data)

        bulk.execute()
    
    except Exception as err:
        return json.dumps({'error_code': 1, 'error_message': err}), 500

    return json.dumps({'error_code': 0, 'error_message': 'None', 'status': 'Import successfully'})

@app.route('/tools/export', methods=['GET'])
def tool_export():
    try:
        if not os.path.exists(file_done_folder):
            os.makedirs(file_done_folder)

        for doc in db.docs_done.find():
            filename = '{}/{}'.format(file_done_folder, doc['doc_name'])
            put_file_content(filename, convert_doc_content_to_write(doc['content']))

        zipf = zipfile.ZipFile('files.zip', 'w', zipfile.ZIP_DEFLATED)
        zipdir(file_done_folder, zipf)
        zipf.close()

    except Exception as err:
        return json.dumps({'error_code': 1, 'error_message': err}), 500

    return send_file('files.zip', attachment_filename='files.zip')

@app.route('/add_relation', methods=['GET', 'POST'])
def add_relation():
    newre = request.form['newre']
    f = open ('relation2id.txt', 'a')
    with open('relation2id.txt') as y:
        lines = sum(1 for line in y)
    f.write(newre + ' ' + str(lines) + '\n')
    f.close()
    return redirect('/')

@app.route('/test', methods=['GET', 'POST'])
def test():
    if request.method=='GET':
        return('<form action="/test" method="post"><input type="submit" value="Send" /></form>')

    elif request.method=='POST':
        return "OK this is a post method"
    else:
        return("ok")

@app.route('/save_doc', methods=['POST'])
def save_doc():
    try:
        content = json.loads(request.form['content'])
        doc_name = (request.form.get('doc_name')).strip()
        # print ('DOC CONTENT', content)
        if doc_name != '':
            db.docs_done.update(
                {
                    'doc_name': doc_name
                },
                {
                    '$set': {
                        'content': content,
                        'updated_at': int(time.time())
                    },
                    '$setOnInsert': {
                        'created_at': int(time.time())
                    }
                }, upsert=True
            )

    except Exception as err:
        return json.dumps({'error_code': 1, 'error_message': err}), 500

    return json.dumps({'error_code': 0, 'error_message': ''})

@app.route('/get_doc/<doc_name>', methods=['GET'])
def get_doc(doc_name):
    doc_detail = {
        'content': '',
        'doc_name': ''
    }
    try:
        docs = db.docs.find_one({
            "doc_name": doc_name
        })

        doc_detail = {
            'doc_name': docs['doc_name'],
            'content': docs['content']
        }
    except Exception as err:
        return json.dumps({'error_code': 1, 'error_message': err}), 500
    return json.dumps({'error_code': 0, 'error_message': '', 'data': doc_detail})


@app.route('/get_doc_done/<doc_name>', methods=['GET'])
def get_doc_done(doc_name):
    doc_detail = {
        'content': '',
        'doc_name': ''
    }
    try:
        docs = db.docs_done.find_one({
            "doc_name": doc_name
        })
        doc_detail = {
            'doc_name': docs['doc_name'],
            'content': docs['content']
        }
    except Exception as err:
        return json.dumps({'error_code': 1, 'error_message': err}), 500

    return json.dumps({'error_code': 0, 'error_message': '', 'data': doc_detail})

if __name__ == '__main__':
    app.secret_key = 'super secret key'
    app.run(port=int(port), host=host, threaded=True)
