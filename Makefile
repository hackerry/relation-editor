build-docker:
	@echo Build docker image of registry.gitlab.com/fi_ai/ne-annotation
	# docker build --rm -t registry.gitlab.com/fi_ai/ne-annotation:`cat VERSION` -t registry.gitlab.com/fi_ai/ne-annotation:latest .
	docker build --rm -t registry.gitlab.com/fi_ai/ne-annotation:latest .

build-docker-alpine:
	@echo Build docker image of registry.gitlab.com/fi_ai/ne-annotation
	docker build --rm -f Dockerfile.alpine.bak -t registry.gitlab.com/fi_ai/ne-annotation:alpine .

build-docker-debian:
	@echo Build docker image of registry.gitlab.com/fi_ai/ne-annotation
	docker build --rm -f Dockerfile.debian.bak -t registry.gitlab.com/fi_ai/ne-annotation:debian .

push-docker-image:
	@echo Push Docker image to registry.gitlab.com
	# docker push registry.gitlab.com/fi_ai/ne-annotation:`cat VERSION`
	docker push registry.gitlab.com/fi_ai/ne-annotation:latest

clean-docker-exited-containers:
	docker rm -v `docker ps -a -q -f status=exited`

clean-none-images:
	docker rmi `docker images | grep none | awk '{print $3}'`

run-docker:
	docker run --rm \
	-p 7000:7000 \
	-it registry.gitlab.com/fi_ai/ne-annotation:latest

run-docker-production:
	docker run -d \
	-p 9002:7000 \
	--restart always \
	-v $HOME/RE-EDITOR/files_done:/app/files_done \
	-v $HOME/RE-EDITOR/files_raw:/app/files_raw \
	registry.gitlab.com/fi_ai/ne-annotation

run:
	MONGODB_HOST=127.0.0.1 MONGODB_PORT=27017 FILE_DONE_FOLDER=files_done FILE_RAW_FOLDER=files_raw python3 main.py
