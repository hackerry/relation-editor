#!/bin/bash

# FROM python:3.6-alpine

# MAINTAINER Tran Duy Thanh <coachtranduythanh@gmail.com>

# ENV LANG C.UTF-8
# ENV APPHOME=/app \
#     FILE_ANNOTATED_FOLDER=/app/files_annotated \
#     FILE_NOT_ANNOTATED_FOLDER=/app/files_not_annotated

# ENV MONGODB_HOST=127.0.0.1 \
#     MONGODB_PORT=27017

# RUN pip3 install flask flask_cors pymongo

# RUN mkdir $APPHOME
# RUN mkdir $APPHOME/files_annotated
# RUN mkdir $APPHOME/files_not_annotated

# COPY templates $APPHOME/templates
# COPY static $APPHOME/static
# COPY main.py $APPHOME

# WORKDIR $APPHOME

# EXPOSE 7000/tcp

# VOLUME ["/app/files_annotated","/app/files_not_annotated"]
# CMD ["python","main.py"]
